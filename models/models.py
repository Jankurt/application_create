# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError

class AteneoApplicant(models.Model):
    _inherit = "hr.applicant"

    job_type = fields.Selection([
        ('regular', 'Regular'),
        ('independent_contactor', 'Independent Contractor'),
        ('part_time', 'Part Time')
    ])

    job_types = fields.Char(compute="get_job_post_dep", store=True)

    department_ids = fields.Char(compute="get_job_post_dep", store=True)

    erf = fields.Many2one(
        'request.employee'
    )

    position = fields.Char(compute="get_job_post_dep", store=True)
    job_ids = fields.Char(compute="get_job_post_dep", store=True)
    manpower = fields.Many2one('hr.plantilla',compute="get_job_post_dep",store=True)
    manpower_name = fields.Char(compute="get_job_post_dep", store=True)
    code = fields.Char()
    id_dep = fields.Char(compute="get_job_post_dep", store=True)

    @api.depends('erf')
    def get_job_post_dep(self):
        for item in self:
            erf_code = item.erf.filtered(lambda  x : x.state == 'validated')
            item.job_types = erf_code.job_type
            if item.job_types == 'regular':
                item.job_types = 'Regular'
            if item.job_types == 'part_time':
                item.job_types = 'Part Time'
            if item.job_types == 'ic':
                item.job_types = 'Independent Contractor'
            if item.job_types == 'project':
                item.job_types = 'Project Based'
            item.department_ids = erf_code.department_id.name
            item.position = erf_code.job_id.name
            item.job_id = erf_code.job_id
            item.manpower = erf_code.plantilla_id
            item.manpower_name = erf_code.plantilla_id.name
            item.id_dep = erf_code.department_id

